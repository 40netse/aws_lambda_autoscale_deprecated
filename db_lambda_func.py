from __future__ import print_function

import json
import boto3

print('Loading function')


def lambda_handler(event, context):
    print("Received event: " + json.dumps(event, indent=2))
    for record in event['Records']:
        print(record['eventID'])
        print(record['eventName'])
        print("DynamoDB Record: " + json.dumps(record['dynamodb'], indent=2))
    c = boto3.client('sns')
    arn = "arn:aws:sns:us-east-1:730386877786:mstack-dynamodb"
    m = json.dumps(event)
    c.publish(TargetArn=arn, Message=m)
    return 'Successfully processed {} records.'.format(len(event['Records']))

