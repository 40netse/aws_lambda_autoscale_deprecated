Instructions for setup
======================
Make sure PYTHONPATH is not set in your .bashrc. It will mess up pip virtualenv
#
# Install AWS account credentials using aws configure
#
aws configure
#
# Install ssh key to checkout code from git
#
git clone https://40netse@bitbucket.org/40netse/aws_lambda_autoscale.git
cd aws_lambda_autoscale
virtualenv venv
source venv/bin/activate
pip install --upgrade pip setuptools
sudo yum install git gcc
#
# Some package dependency problems. So need to install a few packages manually
#
pip install --no-cache-dir lambda-packages 
pip install wheel cryptography zappa
#
# Now install the rest
#
pip install -r requirements.txt

Django Notes
============

settings.py - file for making broad settings changes
urls.py     - match python methods to urls
views.py    - python methods for handling HTTP requests. There can be others, but it starts here.

Instructions for running django locally
=======================================
python manage.py runserver
python manage.py runserver 8080 (for a different port)

edit fgtautoscale settings.py and change ALLOWED_HOSTS for connections from outside
python manage.py runserver 0.0.0.0:8000

Instructions for deploying to Lambda (in progress)
==================================================

zappa init 
	Set deployment options

zappa deploy <stage> (test, dev, prod)
zappa update <stage> (test, dev, prod)


Additional Notes
================

deploy to Lambda
create a Topic in SNS

Subscribe topic to Lambda enpoint. As soon as you subscribe, code needs to be ready to 
respond to subscription request and you should see ARN in subscription request

After you are subscribed, you can generate HTTP messages for testing to lambda endpoint

http POST http://endpoint

After you have everything linked up, configure autoscale group to provide SNS notification
requests via the topic. Setup lifecycle hooks to create lifecycle notifications to another 
URL (or the same if you want to overload the function).
