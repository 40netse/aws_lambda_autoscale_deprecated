import json
import boto3

try:
    from urlparse import urlparse
except ImportError:
    from urllib.parse import urlparse

import re


#from django.shortcuts import get_object_or_404, render
#from django.urls import reverse
#from django.views import View
#from utils import clean_time
#from django.utils import timezone

from django.http import HttpResponseBadRequest, HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from utils import (verify_notification, approve_subscription)
import logging

from . import signals
#from Tasks import process_scheduled

STATUS_OK = 200

VITAL_NOTIFICATION_FIELDS = [
    'Type', 'Message', 'Timestamp', 'Signature',
    'SignatureVersion', 'TopicArn', 'MessageId',
    'SigningCertURL'
]

ALLOWED_TYPES = [
    'Notification', 'SubscriptionConfirmation', 'UnsubscribeConfirmation'
]

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def get_cloudformation_template(data):
    if 'TopicArn' in data:
        arn = data['TopicArn']
        a = arn.split(':')
        topic = a[-1]
        s = topic.split('-')
        stack = s[1]
    client = boto3.client('cloudformation')
    s = client.describe_stacks(StackName=stack)
    code = s['ResponseMetadata']['HTTPStatusCode']
    if code != STATUS_OK:
        return None
    params = s['Stacks'][0]['Parameters']
    for p in params:
        if p['ParameterKey'] == 'DynamoDBTableName':
            tname = p['ParameterValue']
    return stack, tname


def respond_to_subscription_request(request):
    logger.debug("subscription request(): method = %s" % request.method)
    if request.method != 'POST':
        raise Http404

    # If necessary, check that the topic is correct
    if hasattr(settings, 'FGTSCEVT_TOPIC_ARN'):
        # Confirm that the proper topic header was sent
        if 'HTTP_X_AMZ_SNS_TOPIC_ARN' not in request.META:
            return HttpResponseBadRequest('No TopicArn Header')
        #
        # Check to see if the topic is in the settings
        # Because you can have bounces and complaints coming from multiple
        # topics, FGTSCEVT_TOPIC_ARN is a list
        #
        if (not request.META['HTTP_X_AMZ_SNS_TOPIC_ARN']
        in settings.FGTSCEVT_TOPIC_ARN):
            return HttpResponseBadRequest('Bad Topic')

    # Load the JSON POST Body
    if isinstance(request.body, str):
        # requests return str in python 2.7
        request_body = request.body
    else:
        # and return bytes in python 3.4
        request_body = request.body.decode()
    try:
        data = json.loads(request_body)
    except ValueError:
        logger.warning('Notification Not Valid JSON: {}'.format(request_body))
        return HttpResponseBadRequest('Not Valid JSON')
    logger.debug("subscription request(): data = %s" %
                 (json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))))

    # Ensure that the JSON we're provided contains all the keys we expect
    # Comparison code from http://stackoverflow.com/questions/1285911/
    if not set(VITAL_NOTIFICATION_FIELDS) <= set(data):
        logger.warning('Request Missing Necessary Keys')
        return HttpResponseBadRequest('Request Missing Necessary Keys')

    # Ensure that the type of notification is one we'll accept
    if not data['Type'] in ALLOWED_TYPES:
        logger.info('Notification Type Not Known %s', data['Type'])
        return HttpResponseBadRequest('Unknown Notification Type')

    # Confirm that the signing certificate is hosted on a correct domain
    # AWS by default uses sns.{region}.amazonaws.com
    # On the off chance you need this to be a different domain, allow the
    # regex to be overridden in settings
    domain = urlparse(data['SigningCertURL']).netloc
    pattern = getattr(
        settings, 'FGTSCEVT_CERT_DOMAIN_REGEX', r"sns.[a-z0-9\-]+.amazonaws.com$"
    )
    logger.debug("subscription request(): domain = %s, pattern = %s" % (domain, pattern))
    if not re.search(pattern, domain):
        logger.warning(
            'Improper Certificate Location %s', data['SigningCertURL'])
        return HttpResponseBadRequest('Improper Certificate Location')

    # Verify that the notification is signed by Amazon
    if (getattr(settings, 'FGTSCVT_VERIFY_CERTIFICATE', True)
        and not verify_notification(data)):
        logger.error('Verification Failure %s', )
        return HttpResponseBadRequest('Improper Signature')

    # Send a signal to say a valid notification has been received
    signals.notification.send(
        sender='fortigate_autscale', notification=data, request=request)

    # Handle subscription-based messages.
    if data['Type'] == 'SubscriptionConfirmation':
        # Allow the disabling of the auto-subscription feature
        if not getattr(settings, 'BOUNCY_AUTO_SUBSCRIBE', True):
            raise Http404
        return approve_subscription(data)
    elif data['Type'] == 'UnsubscribeConfirmation':
        # We won't handle unsubscribe requests here. Return a 200 status code
        # so Amazon won't redeliver the request. If you want to remove this
        # endpoint, remove it either via the API or the AWS Console
        logger.info('UnsubscribeConfirmation Not Handled')
        return HttpResponse('UnsubscribeConfirmation Not Handled')

    try:
        message = json.loads(data['Message'])
    except ValueError:
        # This message is not JSON. But we need to return a 200 status code
        # so that Amazon doesn't attempt to deliver the message again
        logger.info('Non-Valid JSON Message Received')
        return HttpResponse('Message is not valid JSON')

    logger.debug("subscription request(): message = %s, data = %s" % (message, data))
    return process_message(message, data)


@csrf_exempt
def index(request):
    if isinstance(request.body, str):
        # requests return str in python 2.7
        request_body = request.body
        if request_body is not None and request_body != '':
            try:
                data = json.loads(request_body)
            except ValueError:
                logger.warning('index(): Notification Not Valid JSON: {}'.format(request_body))
                return HttpResponseBadRequest('Not Valid JSON')
    logger.debug("index(): request = %s" %
                 (json.dumps(request.body, sort_keys=True, indent=4, separators=(',', ': '))))
    return HttpResponse('You are not authorized!')

#
# this function is only called via lambda due to a periodic cloudwatch cron
# see zappa_settings.json for configuration
#


@csrf_exempt
def start_scheduled(event):
    logger.debug("start_scheduled(): event = %s" % event)
    return

#
# This routine takes the place of start_scheduled() above, when running in django locally
#


@csrf_exempt
def start(event):
    logger.debug("start(): event = %s" % event)
    if event.method != 'POST':
        raise Http404
    if isinstance(event.body, str):
        request_body = event.body
    try:
        data = json.loads(request_body)
    except ValueError:
        logger.warning('start(): Notification Not Valid JSON: {}'.format(request_body))
        return HttpResponseBadRequest('Not Valid JSON')
    logger.debug("start(): data = %s" % (json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))))
    rc = process_scheduled(event, data)
    return HttpResponse(rc)


@csrf_exempt
def dynamodb(request):
    if isinstance(request.body, str):
        # requests return str in python 2.7
        request_body = request.body
    try:
        data = json.loads(request_body)
    except ValueError:
        logger.warning('dynamodb(): Notification Not Valid JSON: {}'.format(request_body))
        return HttpResponseBadRequest('Not Valid JSON')
    logger.debug("dynamodb(): request = %s" % (json.dumps(data['Message'], sort_keys=True, indent=4, separators=(',', ': '))))
    if request.method == 'POST' and data['Type'] == 'SubscriptionConfirmation':
        return respond_to_subscription_request(request)
    else:
        if 'TopicArn' in data:
            arn = data['TopicArn']
            a = arn.split(':')
            table_name = a[5]
        client = boto3.client('sqs')
        q = client.create_queue(QueueName=table_name)
        gq = client.get_queue_url(QueueName=table_name)
        code = gq['ResponseMetadata']['HTTPStatusCode']
        if code != STATUS_OK:
            return None
        m = json.loads(data['Message'])
        for r in m['Records']:
            client.send_message(QueueUrl=gq['QueueUrl'], MessageBody=json.dumps(r), DelaySeconds=1)
        return HttpResponse(0)


@csrf_exempt
def sns(request):
    """Endpoint that SNS accesses. Includes logic verifying request"""
    # pylint: disable=too-many-return-statements,too-many-branches

    # In order to 'hide' the endpoint, all non-POST requests should return
    # the site's default HTTP404
    if isinstance(request.body, str):
        # requests return str in python 2.7
        request_body = request.body
    try:
        data = json.loads(request_body)
    except ValueError:
        logger.warning('sns(): Notification Not Valid JSON: {}'.format(request_body))
        return HttpResponseBadRequest('Not Valid JSON')
    logger.debug("sns(): request = %s" % (json.dumps(request.body, sort_keys=True, indent=4, separators=(',', ': '))))
    if request.method == 'POST' and data['Type'] == 'SubscriptionConfirmation':
        stack, table_name = get_cloudformation_template(data)
        db = boto3.resource('dynamodb')
        t = db.Table(table_name)
        try:
            i = t.get_item(TableName=table_name, Key={"InstanceId": "0000", "TaskId": "0000"})
        except Exception as myexception:
            t.put_item(Item={"InstanceId": "0000", "TaskId": "0000", "Message": "Start", "Stack": stack})
            return respond_to_subscription_request(request)
        code = i['ResponseMetadata']['HTTPStatusCode']
        if code == 200 and 'Item' in i:
            t.delete_item(TableName=table_name, Key={"InstanceId": "0000", "TaskId": "0000"})
        t.put_item(Item={"InstanceId": "0000", "TaskId": "0000", "Message": "Start", "Stack": stack})
        return respond_to_subscription_request(request)

    if request.method == 'POST' and data['Type'] == 'Notification':
        return HttpResponse(0)


def as_get_instances(asg):
    client = boto3.client('autoscaling', region_name='us-west-2')
    groups = client.describe_auto_scaling_groups()
    for g in groups['AutoScalingGroups']:
        name = g['AutoScalingGroupName']
        n = name.split('-')
        if n[1] == asg:
            instances = g['Instances']
    return instances


def assign_ftp_put(region, bucket, key):
    instance = as_get_instances('FTPASGC')
    print instance
    print region, bucket, key


def dg_process_object_created(r):
    bucket_name = None
    key = None
    region = None
    if 'awsRegion' in r:
        region = r['awsRegion']
    if 'eventSource' in r:
        if r['eventSource'] != 'aws:s3':
            return
    if 's3' in r:
        if 'bucket' in r['s3']:
            if 'name' in r['s3']['bucket']:
                bucket_name = r['s3']['bucket']['name']
        if 'object' in r['s3']:
            if 'key' in r['s3']['object']:
                key = r['s3']['object']['key']
    if region is None or bucket_name is None or key is None:
        return
    assign_ftp_put(region, bucket_name, key)


@csrf_exempt
def dg(request):
    """Endpoint that DG accesses for S3 Dirty Bucket Updates. Includes logic verifying request"""
    # pylint: disable=too-many-return-statements,too-many-branches

    # In order to 'hide' the endpoint, all non-POST requests should return
    # the site's default HTTP404
    if isinstance(request.body, str):
        # requests return str in python 2.7
        request_body = request.body
    try:
        data = json.loads(request_body)
    except ValueError:
        logger.warning('sns(): Notification Not Valid JSON: {}'.format(request_body))
        return HttpResponseBadRequest('Not Valid JSON')
    logger.debug("sns(): request = %s" % (json.dumps(request.body, sort_keys=True, indent=4, separators=(',', ': '))))
    if request.method == 'POST' and data['Type'] == 'SubscriptionConfirmation':
        stack, table_name = get_cloudformation_template(data)
        db = boto3.resource('dynamodb')
        t = db.Table(table_name)
        try:
            i = t.get_item(TableName=table_name, Key={"InstanceId": "0000", "TaskId": "0000"})
        except Exception as myexception:
            t.put_item(Item={"InstanceId": "0000", "TaskId": "0000", "Message": "Start", "Stack": stack})
            return respond_to_subscription_request(request)
        code = i['ResponseMetadata']['HTTPStatusCode']
        if code == 200 and 'Item' in i:
            t.delete_item(TableName=table_name, Key={"InstanceId": "0000", "TaskId": "0000"})
        t.put_item(Item={"InstanceId": "0000", "TaskId": "0000", "Message": "Start", "Stack": stack})
        return respond_to_subscription_request(request)

    if request.method == 'POST' and data['Type'] == 'Notification':
        if isinstance(request.body, str):
            # requests return str in python 2.7
            request_body = request.body
        try:
            data = json.loads(request_body)
        except ValueError:
            logger.warning('sns(): Notification Not Valid JSON: {}'.format(request_body))
            return HttpResponseBadRequest('Not Valid JSON')
        try:
            data2 = json.loads(data['Message'])
        except ValueError:
            logger.warning('sns(): Notification Not Valid JSON: {}'.format(data['Message']))
            return HttpResponseBadRequest('Not Valid JSON')
        if 'Records' in data2:
            for r in data2['Records']:
                if 'eventName' in r:
                    event_type = r['eventName']
                    if event_type == 'ObjectCreated:Put':
                        dg_process_object_created(r)
        return HttpResponse(0)
