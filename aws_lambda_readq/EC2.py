
import boto3
from Constants import *


class EC2(object):
    def __init__(self, instance_id):
        self.instance_id = instance_id
        self.client = boto3.client('ec2')
        self.resource = boto3.resource('ec2')
        self.description = self.client.describe_instance_status(InstanceIds=[self.instance_id])
        self.state = self.description['InstanceStatuses'][0]['InstanceState']['Name']
        self.role = self.get_tag('Fortinet-Role')

    def __repr__(self):
        return ' (%s, %s, %s) ' % (self.instance_id, self.state, self.role)

    def create_eni(self):
        if len(self.fgt.interfaces) > 1:
            return 0

    def format_logdisk(self):
        return 0
        #for instance in self.resource.instances.all():
        #    fgt = Fortigate(self)
        #    status = fgt.get_system_storage(fa, fortigate, "admin", fortigate.instance_id, verbose, debug)
        #    if status != None and status.status_code == STATUS_OK:
        #        value = status.json()
        #        results_len = len(value['results'])
        #        if results_len == 0:
        #            if debug:
        #                print "Fortigate instance %s needs logdisk formatted. This process can take up to 5 minutes..." % fortigate.instance_id
        #            rc = RunExpect(fortigate, 'admin', fortigate.instance_id, "exec formatlogdisk", verbose, debug)
        #            sleep_for_reboot = True
        #print self

    def push_config(self):
        print self

    def get_tag(self, key):
        instances = self.resource.instances.filter(InstanceIds=[self.instance_id])
        for i in instances:
            if i.tags is None:
                return None
            for t in i.tags:
                if t['Key'] == key:
                    return t['Value']
        return None
