#!/usr/bin/env python

import optparse
import sys
import time
import json
import logging
from Message import *

#
#
#
logging.basicConfig(filename='debug.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def main():
    #
    # parse the options
    #
    parser = optparse.OptionParser(version='%prog version 1.0')
    parser.add_option('-P', '--Purge', action="store_true", help='Purge Message Queue')
    parser.add_option('-s', '--sqs', help='sqs queue name')
    (opts, args) = parser.parse_args()
    sqs = opts.sqs
    purge = opts.Purge

    resource = boto3.resource('sqs')
    q = resource.get_queue_by_name(QueueName=sqs)
    #
    # Just for testing. Delete all the messages in the queue so we can start over
    #
    if purge:
        for m in q.receive_messages():
            m.delete()
        return 0

    #
    # This loop is only for Django Development.
    # Actual Lambda code will just register process_message() and handler for dynamodb update
    #
    while 1:
        for msg in q.receive_messages():
            #
            # parse the json and build a message
            #
            m = Message(json.loads(msg.body))
            logger.debug("main(): receive message m.key = %s, m.task = %s, m.name = %s, " % (m.key, m.task, m.name))
            #
            # process the message and if everything goes well, delete the message from the SQS Q
            #
            rc = m.process_message()
            if rc == 0:
                msg.delete()
        time.sleep(1)

if __name__ == "__main__":
    sys.exit(main())