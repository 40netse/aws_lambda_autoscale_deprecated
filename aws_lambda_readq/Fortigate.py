import boto3
from Constants import *


class Fortigate(object):
    def __init__(self, ec2):
        self.ec2 = ec2
        self.device_info = self.ec2.client.describe_instances(InstanceIds=[self.ec2.instance_id])
        self.zone = self.device_info['Reservations'][0]['Instances'][0]['Placement']['AvailabilityZone']
        self.private_ip = None
        self.urt = None
        self.url_prefix = None
        if self.ec2.state == 'terminated' or self.ec2.state == 'stopped':
            return None
        if 'Reservations' in self.device_info:
            if 'Instances' in self.device_info['Reservations'][0]:
                instance = self.device_info['Reservations'][0]['Instances'][0]
                if 'NetworkInterfaces' in instance:
                    self.interfaces = instance['NetworkInterfaces']
                    for interface in instance['NetworkInterfaces']:
                        if interface['SourceDestCheck'] is False:
                            for pip in interface['PrivateIpAddresses']:
                                if pip['Primary'] is True:
                                    self.private_ip = pip['PrivateIpAddress']
        if 'Reservations' in self.device_info:
            if self.private_ip is None:
                self.private_ip = self.device_info['Reservations'][0]['Instances'][0]['PrivateIpAddress']
        self.url_prefix = None
        if self.private_ip is not None:
            self.url_prefix = 'https://' + self.private_ip

    def __repr__(self):
        return ' (%s, %s, %s, instance state = %s, usable route table = %s) ' % \
               (self.ec2.instance_id, self.zone, self.url_prefix, self.ec2.state, self.urt)

    def update_csrf(self):
        # Retrieve server csrf and update session's headers
        csrftoken = None
        for cookie in self.session.cookies:
            if cookie.name == 'ccsrftoken':
                csrftoken = cookie.value[1:-1]
                #
                # bug in FortiOS Rest API. Provide bad credentials and API call return STATUS_OK,
                # but fails and returns a bogus value in csrftoken
                #
                if csrftoken == '0%260':
                    csrftoken = None
                else:
                    self.session.headers.update({'X-CSRFTOKEN': csrftoken})
        return(csrftoken)

    def login(self, name, key, verbose, debug):
        instance = self.fa.ec2client.describe_instances(InstanceIds = [self.instance_id])
        if len(instance['Reservations']) == 0:
            return None
        state = instance['Reservations'][0]['Instances'][0]['State']['Name']
        if state != 'running':
            if debug:
                print "Unable to login to instance %s. Instance is terminated" % self.instance_id
            return None
        url = self.url_prefix + '/logincheck'
        res = None
        login_tries = 3
        while login_tries > 0:
            try:
                res = self.session.post(url, data='username=' + name + '&secretkey=' + key, verify=False)
                if res != None:
                    login_tries = 0
            except Exception, ex:
                print "caught: %s" % ex.message
                if res == None:
                    if verbose:
                        print "Error Fortigate Login()"
                login_tries -= 1

        if res == None:
            if verbose:
                print "Fortigate Login Failed... reason unknown"
            return None
        if (res.status_code != 200) and (res.reason != 'OK'):
            if verbose:
                print "Fortigate Login failed name = %s, key = %s" % (name, key)
            return (res.status_code)
        l = len(res.content)
        #
        # Update session's csrftoken
        #
        token = self.UpdateCsrf()
        if token == None:
            res.status_code = STATUS_NOT_FOUND
        return (res)

    def logout(self):
        url = self.url_prefix + '/logout'
        try:
            res = self.session.post(url)
        except Exception, ex:
            pass
        self.session.close()

    def delete(self, url_postfix, params=None, data=None, verbose=True):
        url = self.url_prefix + url_postfix
        res = self.session.delete(url, params=params, data=data)
        self.UpdateCsrf()
        return(res)

    def get(self, url_postfix, params=None, data=None):
        url = self.url_prefix + url_postfix
        if data is None:
            jdict = None
        else:
            jdict = json.dumps(data)
        res = self.session.get(url, params=params, data = jdict)
        self.UpdateCsrf()
        return(res)

    def put(self, url_postfix, params=None, data=None):
        url = self.url_prefix + url_postfix
        jdict = json.dumps(data)
        res = self.session.put(url, params=params, data=jdict)
        self.UpdateCsrf()
        return(res)

    def post(self, url_postfix, params=None, headers=None, data=None, files=None):
        url = self.url_prefix + url_postfix
        jdict = json.dumps(data)
        res = self.session.post(url, params=params, data=jdict)
        self.UpdateCsrf()
        return (res)

    def get_system_storage(self):
        vdom = 'root'
        params = {'vdom': vdom}
        self.Login(user, password, verbose, debug)
        s = self.get('/api/v2/cmdb/system/storage', params=params)
        self.Logout()
        return s
