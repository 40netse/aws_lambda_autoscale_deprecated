#!/usr/bin/env python

import boto3
import logging
import re

from Constants import *
from EC2 import EC2
from Fortigate import Fortigate

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def get_cft_instances(stack):
    instances = []
    client = boto3.client('cloudformation')
    resources = client.list_stack_resources(StackName=stack)
    if 'ResponseMetadata' not in resources:
        return None
    code = resources['ResponseMetadata']['HTTPStatusCode']
    if code != STATUS_OK and 'StackResourceSummaries' not in resources:
        return None
    for r in resources['StackResourceSummaries']:
        if r['ResourceType'] == 'AWS::EC2::Instance':
            instances.append(r['PhysicalResourceId'])
    return instances


class Message(object):
    def __init__(self, r):
        if r is None:
            return None
        self.r = r
        self.name = self.stack = self.key = self.task = self.arn = self.table_name = None
        if 'eventName' in self.r:
            self.name = self.r['eventName']
        if 'dynamodb' in self.r:
            self.key = self.r['dynamodb']['Keys']['InstanceId']['S']
            self.task = self.r['dynamodb']['Keys']['TaskId']['S']
            if 'NewImage' in self.r['dynamodb']:
                if 'Stack' in self.r['dynamodb']['NewImage']:
                    self.stack = self.r['dynamodb']['NewImage']['Stack']['S']
        if 'eventSourceARN' in self.r:
            self.arn = self.r['eventSourceARN']
            a = self.arn.split(':')
            tf = a[5].split('/')
            self.table_name = tf[1]
        logger.debug("Message object: arn = %s, key = %s, task = %s, event name = %s" %
                     (self.arn, self.key, self.task, self.name))

    def process_start_message(self):
        logger.debug("process_start_message(): processed dynamodb INSERT start record")
        instances = get_cft_instances(self.stack)
        db = boto3.resource('dynamodb')
        t = db.Table(self.table_name)
        item = t.get_item(Key={"InstanceId": RESOURCE_MSG, "TaskId": DEFAULT_TASK_ID})
        if 'Item' not in item:
            t.put_item(Item={"InstanceId": RESOURCE_MSG, "TaskId": DEFAULT_TASK_ID, "EC2 Instances": instances})
        for i in instances:
            item = t.get_item(Key={"InstanceId": i, "TaskId": TASK_CREATE_ENI})
            if 'Item' not in item:
                t.put_item(Item={"InstanceId": i, "TaskId": TASK_CREATE_ENI})
        return

    def process_message(self):
        #
        # Nothing to do for start messages or
        #
        if self.name == 'REMOVE':
            return 0
        if self.name == 'REMOVE' and (self.key == START_MSG or self.key == RESOURCE_MSG):
            return 0
        if self.key == RESOURCE_MSG:
            return 0
        #
        # The start message was processed and we need to find the stack resources and initialize them
        #
        if self.key == START_MSG and self.task == DEFAULT_TASK_ID and self.name == 'INSERT':
            self.process_start_message()
            return 0
        ec2 = EC2(self.key)
        if ec2 is None:
            return 0
        ec2.fgt = Fortigate(ec2)
        if ec2.fgt is None:
            return 0
        m1 = m2 = None
        m1 = re.match("^PrimaryFirewall?", ec2.role)
        m2 = re.match("^OnDemandFirewall?", ec2.role)
        db = boto3.resource('dynamodb')
        t = db.Table(self.table_name)
        if m1 is None and m2 is None:
            return -1
        if ec2.state != 'running':
            return -1
        if self.task == TASK_CREATE_ENI:
            rc = ec2.create_eni()
            if rc == 0:
                t.delete_item(Key={"InstanceId": self.key, "TaskId": TASK_CREATE_ENI})
                t.put_item(Item={"InstanceId": self.key, "TaskId": TASK_FORMAT_LOGDISK})
                return 0
        if self.task == TASK_FORMAT_LOGDISK:
            rc = ec2.format_logdisk()
            if rc == 0:
                t.delete_item(Key={"InstanceId": self.key, "TaskId": TASK_FORMAT_LOGDISK})
                t.put_item(Item={"InstanceId": self.key, "TaskId": TASK_PUSH_CONFIG})
                return 0
        if self.task == TASK_PUSH_CONFIG:
            rc = ec2.push_config()
            if rc == 0:
                t.delete_item(Key={"InstanceId": self.key, "TaskId": TASK_PUSH_CONFIG})
                return 0
        return 0
